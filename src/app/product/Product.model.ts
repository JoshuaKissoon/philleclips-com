/**
 * Class representation of a Product
 * 
 * @author Joshua Kissoon
 * @since 20170122
 */
export class Product {
    pid: number;
    title: string;
    description: string;
    sku: string;
    powerFactor: string;
    operatingTemperature: string;
    lightPattern: string;
    driverEfficiency: string;
    luminousEfficiency: string;
    material: string;
    cri: string;
    weight: string;
    ipRating: string;
    createdTs: string;
    updatedTs: string;
}
