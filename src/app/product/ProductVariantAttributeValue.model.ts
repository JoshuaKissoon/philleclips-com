/**
 * Class with the Attribute value for a product variant
 * 
 * @author Joshua Kissoon
 * @since 20170123
 */
export class ProductVariantAttributeValue
{
    pvavid: number;
    pvid: number;
    aid: number;
    value: string;
    
    /* Related */
    attribute: string;
}
