import {ProductVariantAttributeValue} from './ProductVariantAttributeValue.model';

/**
 * Class representation of a Product Variant
 * 
 * @author Joshua Kissoon
 * @since 20170123
 */
export class ProductVariant
{
    pvid: number;
    pid: number;
    price: number;
    description: string;
    createdTs: string;
    updatedTs: string;

    attributeValues: ProductVariantAttributeValue[];

    loadFromMap(jsonData: any)
    {
        this.pvid = jsonData.pvid;
        this.pid = jsonData.pid;
        this.price = jsonData.price;
        this.description = jsonData.description;
        this.attributeValues = jsonData.attributeValues;
    }

    getAttributeValue(aid: number): string
    {
        for (let attributeValue of this.attributeValues)
        {
            if (attributeValue.aid == aid)
            {
                return attributeValue.value;
            }
        }
        
        return "-";
    }
}
