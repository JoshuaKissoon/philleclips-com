/**
 * Class representation of a Product Attribute
 * 
 * @author Joshua Kissoon
 * @since 20170122
 */
export class ProductImage
{
    piid: number;
    pid: number;
    fileName: number;
    createdTs: string;
    updatedTs: string;
    imageUrl: string;
}
