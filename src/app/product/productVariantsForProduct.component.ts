import {Component, OnInit, Input} from '@angular/core';

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {Product} from './Product.model';
import {ProductVariant} from './ProductVariant.model';
import {ProductAttribute} from './ProductAttribute.model';

/**
 * Controller to manage the Product Variants for a specific Product
 * 
 * @author Joshua Kissoon
 * @since 20170125
 */
@Component({
    "selector": "product-variants-for-product",
    "templateUrl": "product-variants-for-product.html"
})
export class ProductVariantsForProduct implements OnInit
{
    @Input() product: Product;

    private variants: ProductVariant[];
    private productAttributes: ProductAttribute[];
    private filters: any;

    constructor(private dataManager: DataManager, private alertService: AlertService)
    {
        this.filters = {};
        this.filters.sortField = "pvid";
        this.filters.sortOrder = "ASC";
    }

    ngOnInit()
    {
        this.loadProductAttributes();
        this.loadData();
    }

    loadProductAttributes()
    {
        this.dataManager.POST("product/product/attribute", {"pid": this.product.pid}).then(
            res =>
            {
                if (res.success == false)
                {
                    this.alertService.addAlert(res.message, "warning");
                    return;
                }

                this.productAttributes = res.data.items;
            }
        );
    }

    loadData()
    {
        this.filters.pid = this.product.pid;
        this.dataManager.POST("product/variant", this.filters).then(
            res =>
            {
                if (res.success == false)
                {
                    this.alertService.addAlert(res.message, "warning");
                    return;
                }

                this.variants = new Array();
                res.data.items.forEach((item, index) =>
                {
                    var pv = new ProductVariant();
                    pv.loadFromMap(item);
                    this.variants.push(pv);
                });
            }
        );
    }
}
