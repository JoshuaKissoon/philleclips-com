import {Component, OnInit} from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {Product} from './Product.model';

/**
 * Component to view the set of Products
 * 
 * @author Joshua Kissoon
 * @since 20170122
 */
@Component({
    selector: 'product-product-view-all',
    templateUrl: 'products-listing.html'
})
export class ProductProductsListing implements OnInit
{
    private products: Product[];
    private filters: any;

    constructor(private dataManager: DataManager, private alertService: AlertService,
        private loadingBar: SlimLoadingBarService)
    {
        this.filters = {};
        this.filters.sortField = "title";
        this.filters.sortOrder = "ASC";
    }

    ngOnInit()
    {
        this.loadData();
    }

    loadData()
    {
        this.loadingBar.start();
        this.dataManager.POST("public/product/product", this.filters).then(
            res =>
            {
                if (res.success == false)
                {
                    this.alertService.addAlert(res.message, "warning");
                    return;
                }

                this.products = res.data.items;
                this.loadingBar.complete();
            }
        );
    }
}