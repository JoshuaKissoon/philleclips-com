import {Component, OnInit, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {Product} from './Product.model';
import {ProductAttribute} from './ProductAttribute.model';

/**
 * Component used to view a product
 * 
 * @author Joshua Kissoon
 * @since 20170122
 */
@Component({
    selector: 'product-product-view',
    templateUrl: 'product-view.html'
})
export class ProductProductView implements OnInit, AfterViewInit
{

    private product = new Product();
    private attributes: ProductAttribute[];

    constructor(private dataManager: DataManager, private alertService: AlertService,
        private route: ActivatedRoute, private loadingBar: SlimLoadingBarService) {}

    ngOnInit()
    {
        this.loadProduct();
    }

    loadProduct()
    {
        this.loadingBar.start();
        this.route.params.subscribe(params =>
        {
            if (params['pid'] !== undefined)
            {

                this.dataManager.GET("product/product/" + params['pid']).then(
                    res =>
                    {
                        if (res.success != true)
                        {
                            this.alertService.addAlert(res.message, "warning");
                            return;
                        }
                        this.product = res.data.item;
                        this.loadingBar.complete();
                    }
                );
            }
        });
    }

    ngAfterViewInit()
    {

    }

}