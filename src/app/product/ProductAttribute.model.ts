/**
 * Class representation of a Product Attribute
 * 
 * @author Joshua Kissoon
 * @since 20170122
 */
export class ProductAttribute {
    paid: number;
    pid: number;
    aid: number;
    attribute: string;
    measure: string;
}
