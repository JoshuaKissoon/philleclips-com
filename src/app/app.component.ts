import {Component} from '@angular/core';

import {AlertService} from './services/AlertService.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    providers: [AlertService]
})
export class AppComponent
{

    constructor(private alertService: AlertService)
    {

    }
    title = 'app works!';
}
