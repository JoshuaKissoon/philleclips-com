import {Injectable} from "@angular/core";

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {CartItem} from "./CartItem.model";

/**
 * TS Class to manage a user Cart
 * 
 * All data is stored in local storage
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
@Injectable()
export class Cart
{

    public static STORAGE_DATA_KEY = "philleclips_cart_items";

    private items: CartItem[] = new Array();

    constructor(private dataManager: DataManager, private alertService: AlertService)
    {
        if (localStorage.getItem(Cart.STORAGE_DATA_KEY) == null)
        {
            this.saveItems();
        }
    }

    public addItem(newItem: CartItem)
    {
        this.loadItems();

        for (let item of this.items)
        {
            if (item.pvid == newItem.pvid)
            {
                item.quantity += newItem.quantity;
                return this.saveItems();
            }
        }

        this.items.push(newItem);
        return this.saveItems();
    }

    public updateItem(newItem: CartItem)
    {
        this.loadItems();

        for (let item of this.items)
        {
            if (item.pvid == newItem.pvid)
            {
                item.quantity = newItem.quantity;
                return this.saveItems();
            }
        }

        this.items.push(newItem);
        return this.saveItems();
    }

    removeItem(pvid: number)
    {
        this.loadItems();

        for (let item of this.items)
        {
            if (item.pvid == pvid)
            {
                this.items.splice(this.items.indexOf(item), 1);
            }
        }

        return this.saveItems();
    }

    private loadItems()
    {
        this.items = new Array();

        var rawData = JSON.parse(localStorage.getItem(Cart.STORAGE_DATA_KEY));

        for (let row of rawData)
        {
            var item = new CartItem();
            item.loadFromMap(row);
            this.items.push(item);
        }

        return this.items;
    }

    public getItems(): CartItem[]
    {
        this.loadItems();
        return this.items;
    }

    private saveItems()
    {
        var success = localStorage.setItem(Cart.STORAGE_DATA_KEY, JSON.stringify(this.items));

        return success;
    }


    clear()
    {
        this.items = new Array();

        return this.saveItems();
    }
}