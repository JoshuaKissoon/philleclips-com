import {Component, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {ReCaptchaComponent} from 'angular2-recaptcha/lib/captcha.component';

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {CustomerInfo} from "./CustomerInfo.model";
import {Cart} from "./Cart.service";

/**
 * Component to load and view the cart
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
@Component({
    "selector": "checkout-page",
    "templateUrl": "./checkout-page.html"
})
export class CheckoutPageComponent implements OnInit
{

  @ViewChild(ReCaptchaComponent) captcha:ReCaptchaComponent;
  
    private customer = new CustomerInfo();
    public static CUSTOMER_INFO_STORAGE_DATA_KEY = "philleclips_checkout_customer_info";
    private captchaResponse: any;

    cartFormClass = "active";
    contactFormClass = "";
    finalizeFormClass = "";

    constructor(private dataManager: DataManager, private alertService: AlertService,
        private cart: Cart, private router: Router)
    {
        if (localStorage.getItem(CheckoutPageComponent.CUSTOMER_INFO_STORAGE_DATA_KEY) == null)
        {
            this.saveCustomerInfo();
        }
        else
        {
            this.loadCustomerInfo();
        }
    }

    ngOnInit()
    {
    }

    showCartForm()
    {
        this.cartFormClass = "active";
        this.contactFormClass = "";
        this.finalizeFormClass = "";
    }
    showContactForm()
    {
        this.cartFormClass = "";
        this.contactFormClass = "active";
        this.finalizeFormClass = "";
    }
    showFinalizeForm()
    {
        this.cartFormClass = "";
        this.contactFormClass = "";
        this.finalizeFormClass = "active";
        this.saveCustomerInfo();
    }

    saveCustomerInfo()
    {
        var success = localStorage.setItem(CheckoutPageComponent.CUSTOMER_INFO_STORAGE_DATA_KEY, JSON.stringify(this.customer));

        return success;
    }

    loadCustomerInfo()
    {
        var rawData = JSON.parse(localStorage.getItem(CheckoutPageComponent.CUSTOMER_INFO_STORAGE_DATA_KEY));

        this.customer.loadFromMap(rawData);

        return this.customer;
    }

    handleCaptchaResponse(response: any)
    {
        this.captchaResponse = response;
    }
    
    requestQuotation()
    {
        var data = {
            "products": this.cart.getItems(),
            "customer": this.customer,
            "captchaResponse": this.captchaResponse,
        };

        this.dataManager.PUT("quotation/quotation", data).then(
            res =>
            {
                if (res.success != true)
                {
                    this.alertService.addAlert(res.message, "warning");
                    return;
                }

                this.alertService.addAlert(res.message, "success");
                this.cart.clear();
                this.router.navigate(["home"]);
            }
        );
    }


}
