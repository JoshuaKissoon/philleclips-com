import {Component, OnInit} from "@angular/core";
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import {DataManager} from '../services/DataManager.service';
import {AlertService} from '../services/AlertService.service';

import {CartItem} from "../cart/CartItem.model";
import {Cart} from "./Cart.service";

/**
 * Component to load and view the cart
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
@Component({
    "selector": "cart-view",
    "templateUrl": "./cart-view.html"
})
export class CartViewComponent implements OnInit
{

    private cartItems: CartItem[];

    constructor(private dataManager: DataManager, private alertService: AlertService,
        private cart: Cart, private loadingBar: SlimLoadingBarService) {}

    ngOnInit()
    {
        this.cartItems = this.cart.getItems();
        this.loadData();
    }

    loadData()
    {
        this.loadingBar.start();
        var pvids = Array();

        for (let item of this.cart.getItems())
        {
            pvids.push(item.pvid);
        }

        this.dataManager.POST("product/variant", {"pvid": pvids}).then(
            res =>
            {
                if (res.success == false)
                {
                    this.alertService.addAlert(res.message, "warning");
                    return;
                }

                for (let row of res.data.items)
                {
                    for (let cartItem of this.cartItems)
                    {
                        if (cartItem.pvid == row.pvid)
                        {
                            cartItem.variant = row;
                        }
                    }

                }

                this.loadingBar.complete();
            }
        );
    }

    removeItem(pvid: number)
    {
        this.cart.removeItem(pvid);

        /* Remove the item from our local cartItems data */
        for (let item of this.cartItems)
        {
            if (item.pvid == pvid)
            {
                this.cartItems.splice(this.cartItems.indexOf(item), 1);
            }
        }
    }
}
