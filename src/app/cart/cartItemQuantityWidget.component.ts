import {Component, Input, OnInit} from "@angular/core";

import {AlertService} from "../services/AlertService.service";

import {Cart} from "./Cart.service";
import {CartItem} from "./CartItem.model";

/**
 * Component used to manage the quantity of a cart item
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
@Component({
    "selector": "cart-item-quantity-widget",
    "templateUrl": "./cart-item-quantity-widget.html"
})
export class CartItemQuantityWidgetComponent implements OnInit
{

    @Input() pvid: number;

    private item = new CartItem();

    constructor(private cart: Cart, private alertService: AlertService)
    {
        this.item.quantity = 1;
    }

    ngOnInit()
    {
        /* Get the item cart data */
        for (let cartItem of this.cart.getItems())
        {
            if (this.pvid == cartItem.pvid)
            {
                this.item = cartItem;
            }
        }

    }

    reduceQuantity()
    {
        this.item.quantity--;
        this.saveItem();
    }

    increaseQuantity()
    {
        this.item.quantity++;
        this.saveItem();
    }

    saveItem()
    {
        this.item.pvid = this.pvid;

        if (this.cart.updateItem(this.item))
        {
            this.alertService.addAlert("Successfully updated the item to your cart. ", "success");
            return;
        }

        this.alertService.addAlert("Failed to add the item to your cart, please refresh the page and try again. ", "warning");
    }
}