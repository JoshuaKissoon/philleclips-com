import {Component, Input} from "@angular/core";

import {CustomerInfo} from "./CustomerInfo.model";
/**
 * Component used to collect customer information for the checkout process
 * 
 * @author Joshua Kissoon
 * @since 20170131
 */
@Component({
    "selector": "customer-info-form",
    "templateUrl": "./customer-info-form.html"
})
export class CustomerInfoFormComponent
{
    @Input() customer: CustomerInfo;

}