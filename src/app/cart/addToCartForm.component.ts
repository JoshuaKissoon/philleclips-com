import {Component, Input} from "@angular/core";

import {AlertService} from "../services/AlertService.service";

import {Cart} from "./Cart.service";
import {CartItem} from "./CartItem.model";

/**
 * Component used to add an item to the cart
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
@Component({
    "selector": "add-to-cart-form",
    "templateUrl": "./add-to-cart-form.html"
})
export class AddToCartFormComponent
{

    @Input() pvid: number;

    private item = new CartItem();

    constructor(private cart: Cart, private alertService: AlertService)
    {
        this.item.quantity = 1;
    }

    saveItem()
    {
        this.item.pvid = this.pvid;

        if (this.cart.addItem(this.item))
        {
            this.alertService.addAlert("Successfully added the item to your cart. ", "success");
            return;
        }

        this.alertService.addAlert("Failed to add the item to your cart, please refresh the page and try again. ", "warning");
    }
}