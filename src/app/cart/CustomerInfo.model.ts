/**
 * Model used to store customer information
 * 
 * @author Joshua Kissoon
 * @since 20170131
 */
export class CustomerInfo
{
    named: string;
    email: string;
    phone: string;
    address: string;
    company: string;
    comment: string;

    loadFromMap(dataMap: any)
    {
        this.named = dataMap.named;
        this.email = dataMap.email;
        this.phone = dataMap.phone;
        this.address = dataMap.address;
        this.company = dataMap.company;
        this.comment = dataMap.comment;
    }
}