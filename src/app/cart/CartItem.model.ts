import {ProductVariant} from "../product/ProductVariant.model";

/**
 * Class representation of a cart item
 * 
 * @author Joshua Kissoon
 * @since 20170127
 */
export class CartItem
{
    ciid: number;
    pvid: number;
    quantity: number;

    variant = new ProductVariant();
    
    loadFromMap(dataMap: any)
    {
        this.pvid = dataMap.pvid;
        this.quantity = dataMap.quantity;
    }
}