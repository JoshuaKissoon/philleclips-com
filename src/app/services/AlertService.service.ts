/**
 * Service used to manage alerts in the entire system frontend
 * 
 * @author Joshua Kissoon
 * @since 20161013
 */
import {Injectable} from "@angular/core";

@Injectable()
export class AlertService {
    private alerts: any = [];

    constructor() {
        this.alerts = [];
    }

    addAlert(message: string, alertType: string) {
        this.alerts.push({ "message": message, "type": alertType });
    }

    getAlerts() {
        return this.alerts;
    }

    removeAlert(message: string) {
        for (var key in this.alerts) {
            var alert = this.alerts[key];

            if (alert.message == message) {
                this.alerts.splice(key, 1);
                break;
            }

        }
    }
}
