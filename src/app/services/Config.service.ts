import {Injectable} from '@angular/core';

/**
 * Service that provides settings for all other services
 * 
 * @author Joshua Kissoon
 * @since 20161202
 */
@Injectable()
export class Config {
    getBaseUrl() {
        return "http://localhost/philleclipsadminapi/?urlq=";
    }
}