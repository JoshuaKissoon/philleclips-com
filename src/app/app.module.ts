import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';


import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import { ReCaptchaModule } from 'angular2-recaptcha';

import {AppComponent} from './app.component';

import {enableProdMode} from '@angular/core';

enableProdMode();

import {RoutingProvider} from './app.routes';


import {Config} from './services/Config.service';
import {DataManager} from './services/DataManager.service';
import {AlertService} from './services/AlertService.service';
import {Cart} from './cart/Cart.service';

import {HomeComponent} from "./home/home.component";
import {SearchBoxComponent} from "./search/searchBox.component";
import {ProductProductsListing} from "./product/productsListing.component";
import {ProductProductView} from "./product/productView.component";
import {ProductVariantsForProduct} from "./product/productVariantsForProduct.component";
import {AddToCartFormComponent} from "./cart/addToCartForm.component";
import {CartViewComponent} from "./cart/cartView.component";
import {CheckoutPageComponent} from "./cart/checkoutPage.component";
import {CartItemQuantityWidgetComponent} from "./cart/cartItemQuantityWidget.component";
import {CustomerInfoFormComponent} from "./cart/customerInfoForm.component";

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        SearchBoxComponent,
        ProductProductsListing, ProductProductView,
        ProductVariantsForProduct,
        AddToCartFormComponent, CartViewComponent, CheckoutPageComponent, 
        CartItemQuantityWidgetComponent, CustomerInfoFormComponent,
    ],
    imports: [BrowserModule, FormsModule, HttpModule, RoutingProvider, SlimLoadingBarModule.forRoot(), ReCaptchaModule],
    bootstrap: [AppComponent],
    providers: [Config, DataManager, AlertService, Cart]
})
export class AppModule {}
