import {RouterModule, Routes} from '@angular/router';

import { HomeComponent } from './home/home.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
];

/* Products Management */
import { ProductProductsListing } from "./product/productsListing.component";
routes.push({ path: 'product/all', component: ProductProductsListing });
import { ProductProductView } from "./product/productView.component";
routes.push({ path: 'product/:pid/view', component: ProductProductView });

/* Cart & Checkout management */
import {CheckoutPageComponent} from "./cart/checkoutPage.component";
routes.push({ path: 'checkout', component: CheckoutPageComponent });

export const RoutingProvider = RouterModule.forRoot(routes, { useHash: true });