import { PhilleclipsComPage } from './app.po';

describe('philleclips-com App', function() {
  let page: PhilleclipsComPage;

  beforeEach(() => {
    page = new PhilleclipsComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
